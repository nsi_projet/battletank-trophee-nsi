
import pygame
import time 
import sys
import math
import random

class Objet(): #on définit une classe Objet pour toutes les instances qui sont des objets
    def __init__(self, image,x,y,angle): #on définit une image, une position et un angle
        self.image = pygame.transform.rotate(pygame.image.load(image),angle)
        self.rect = self.image.get_rect()
        self.rect.center = (x,y)
    
    def changer_position(self,x,y):
        self.rect.center = (x,y)
    
    def cliquer(self): #Pour tester si lees objets sont cliqués
        if event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1 and self.rect.collidepoint(event.pos):
                return True

class Tank(): #une classe tank pour les 2 tanks
    def __init__(self, image,x, y,angle, vitesse):
        self.original_image = pygame.image.load(image) #pour definir l'image des tank
        self.original_center = (x,y)
        self.original_angle = angle
        self.image =self.original_image #On créer une différence entre l'image de base et l'image actuelle
        self.rect = self.image.get_rect()
        self.rect.center= (x, y) #position du tank
        self.original_vitesse = vitesse
        self.vitesse = self.original_vitesse
        self.angle = angle #angle de base

        self.score = 0 #le score de chaque tanks
        self.explosion = False #définir un état du tank, en explosion ou pas en explosion
        self.temps_dernier_tir= time.time() #des variables de temps utilisés plus tard
        self.temps_explosion = time.time()
    
    def move_tank(self, bool):
    # bool définie si les flêches directionelles sont zqsd ou les flèches du clavier selon le tank
        if bool:
            up, down, right, left = pygame.K_UP, pygame.K_DOWN, pygame.K_RIGHT, pygame.K_LEFT
        else:
            up, down, right, left = pygame.K_z, pygame.K_s, pygame.K_d, pygame.K_q
    # calcul du deplacement du tank en fonction de touche préssée
        dx, dy = 0, 0
        if pygame.key.get_pressed()[left]:
            self.angle += vitesse_rotation
        elif pygame.key.get_pressed()[right]:
            self.angle -= vitesse_rotation
        if pygame.key.get_pressed()[up]:
            dx = math.cos(math.radians(self.angle))
            dy = math.sin(math.radians(self.angle)) * -1
        elif pygame.key.get_pressed()[down]:
            dx = math.cos(math.radians(self.angle)) * -1
            dy = math.sin(math.radians(self.angle))
        
    # pour que le tank ne sorte pas de la fenêtre
        if self.rect.right >= window_width-30:
            self.rect.x = window_width-30-self.rect.width
        if self.rect.left <= 30:
            self.rect.x = 30
        if self.rect.bottom >= window_height-30:
            self.rect.y = window_height-30-self.rect.height
        if self.rect.top <= 30:
            self.rect.y = 30
        
    # deplacement du tank
        self.rect.x += dx*self.vitesse
        self.rect.y += dy*self.vitesse

    def tire_ou_pas(self,bool):
    # on definie la touche pour tirer selon le tank
        if bool:
            touche_tirer= pygame.K_SPACE
        else:
            touche_tirer= pygame.K_LSHIFT

    # verifie si 2s se sont passé depuis le dernier tir puis crée l'objet projectile
        if pygame.key.get_pressed()[touche_tirer] and temps_actuel - self.temps_dernier_tir >= 2:
            tir_song.play() #on lance le son du tir
            if mode == 2: #si le mode est le mode "solo", on rajoute 1 points à chauqe tirs
                tank2.augmente_score()
            bullet = Tir("sprites/bullet.png", self.rect.centerx, self.rect.centery,vitesse_bullet,self.angle)
            bullets.append(bullet) #ajoute ce bullet à la liste des bullets
            self.temps_dernier_tir = temps_actuel #actualiser le temps du dernier tir

    def bloque_tank(self): #si un tank est touché par un bullet
        for explosion in explosions:
            screen.blit(explosion, (self.rect.centerx-50 , self.rect.centery-50)) #on affiche l'image de l'explosion sur le tank touché
        self.change_vitesse(0) #on bloque le tank en mettant sa vitesse à 0

    def debloque_tank(self): 
        explosions.remove(explosion_image) #on enlève l'image de l'explosion du tank
        self.explosion = False
        if mode == 1: # si c'est le mode pièces, on reinitialise les vitesse, la position et l'angle uniquement du tank touché
            self.vitesse = self.original_vitesse
            self.rect.center = self.original_center
            self.angle = self.original_angle 
        elif mode == 0: # si c'est le mode classique, on reinitialise les 3 donnés mais pour les 2 tanks avec une fonction reinitiaser_jeu
            self.reinitialiser_jeu()

    def visualiser(self, angle):# pour visualiser le tank sur l'ecran, avec le bon angle
        self.image = pygame.transform.rotate(self.original_image,angle) #fonction pygame qui rotationne l'objet
        self.rect = self.image.get_rect(center=self.rect.center) #on remet l'image au bon endroit
        screen.blit(self.image, self.rect) #on affiche le tank

    def visualiser_score(self,coef): #pour visualiser le score
        score_actuelle= img_score[self.score] #on récupère la bonne image de score dans notre liste d'image de score
        if mode != 2:
            score_actuelle.rect.x = window_width//2 + 150*coef #selon le score de quel tank afficher, le coef détermine si il affiche à -150 du centre ou +150
        else:
            score_actuelle.rect.x = window_width//2 + 75*coef #si c'est le mode solo, on rapproche les scores pour qu'ils ne forment q'un nombre
        screen.blit(score_actuelle.image, score_actuelle.rect)

    def augmente_score(self):
        if self.score < 9: #fonction d'augmentation des scores
            self.score += 1
        else: #utilisé pour le mode solo, si on dépasse 9 sur le score de droite, on rajoutte 1 sur le score de gauche et met à 0 de score de droite (pour passer à une dizaine supplémentaire)
            tank1.score += 1
            tank2.score = 0

    def change_vitesse(self, vitesse):
        #augmente la vitesse du tank 
        self.vitesse = vitesse

    def verif_obj(self):
        #Pour le mode pièces
        if self.rect.colliderect(coin.rect):#si un tank va sur une pièce
            self.score += 1 #il gagne un point
            coin.changer_position(random.randint(30,1330), random.randint(30,670)) #la pièce change sa position

    def reinitialiser_jeu(self): #tout réinitialiser si c'est le mode classique
        tank1.rect.center = tank1.original_center
        tank1.angle = tank1.original_angle
        tank1.vitesse = tank1.original_vitesse
        tank2.rect.center = tank2.original_center
        tank2.angle = tank2.original_angle
        tank2.vitesse = tank2.original_vitesse
        bullets.clear()

    def verif_bullet(self, bool): #pour verifier la colosion entre les tanks et les bullets
        for bullet in bullets:
            if bullet.rect.colliderect(self.rect): #si colision
                if mode == 0: #si c'est le mode classique, le tank touché donne 1 point à son adversaire
                    if bool == 1:
                        tank1.augmente_score()
                    else:
                        tank2.augmente_score()
                elif mode == 1: #si c'est le mode pièce, le tank touché perd 1 point
                    if self.score >= 1: #on vérifie que le score soit supérieur à 0 avant d'enlever 1
                        self.score -= 1
                self.explosion = True #le tank passe en état d'explosion
                explosions.append(explosion_image) #on ajoute l'image d'explosion à la liste
                bullets.remove(bullet) #on supprime la bullet qui à touché le tank
                self.temps_explosion = temps_actuel #on met le temps d'explosion à jour pour faire durer l'explosion 1.5s

class Tir(): #la dérnière classe pour les objets bullets
    def __init__(self, image, x, y, speed,angle_tank):                  
        self.image = pygame.image.load(image)
        self.rect = self.image.get_rect()
        self.dx = math.cos(math.radians(angle_tank))
        self.dy = math.sin(math.radians(angle_tank)) * -1
        self.rect.center = (x+self.dx*70, y+self.dy*70)
        self.speed = speed

    def move(self): #le déplacements des objets bullets
        if self.rect.right >= window_width-30 or self.rect.left <= 30: #on inverse l'angle du bullet si il touche un mur
            self.dx *= -1
        if self.rect.bottom >= window_height-30 or self.rect.top <= 30:
            self.dy *= -1

        if self.rect.right >= window_width-30: #pour éviter des beugs, on déplaces le bullet au niveau du mur si il le dépasse
            self.rect.x = window_width-30-self.rect.width
        if self.rect.left <= 30:
            self.rect.x = 30
        if self.rect.bottom >= window_height-30:
            self.rect.y = window_height-30-self.rect.height
        if self.rect.top <= 30:
            self.rect.y = 30

        self.rect.x += self.dx * self.speed #on change les coordonés du bullet pour le faire avançer
        self.rect.y += self.dy * self.speed

#Initialisation de Pygame
pygame.init()

#Paramètres de la fenêtre
window_width, window_height = 1370, 695
screen = pygame.display.set_mode((window_width, window_height))
pygame.display.set_caption("TANK MIEUX")

#données variables selon les préférences du joueur par exemple
vitesse_rotation = 7
vitesse_tank = 7
vitesse_bullet = 8

#Charger images:
#on commence par initier les instances des 2 tanks
tank1 = Tank("sprites/tank1.png",100,window_height-100,30,vitesse_tank)
tank2 = Tank("sprites/tank2.png",window_width-100,100,210,vitesse_tank)

#initiation des instances de la classe Objet
img_back = Objet("sprites/back.jpg",window_width//2, window_height//2,0)
img_0 = Objet("sprites/0.png",window_width//2 , 80 ,0)
img_1 = Objet("sprites/1.png",window_width//2 , 80 ,0)
img_2 = Objet("sprites/2.png",window_width//2 , 80 ,0)
img_3 = Objet("sprites/3.png",window_width//2 , 80 ,0)
img_4 = Objet("sprites/4.png",window_width//2 , 80 ,0)
img_5 = Objet("sprites/5.png",window_width//2 , 80 ,0)
img_6 = Objet("sprites/6.png",window_width//2 , 80 ,0)
img_7 = Objet("sprites/7.png",window_width//2 , 80 ,0)
img_8 = Objet("sprites/8.png",window_width//2 , 80 ,0)
img_9 = Objet("sprites/9.png",window_width//2 , 80 ,0)
classique = Objet("sprites/bois_classique_.png",window_width//2+100, window_height//2 - 25,0)
classique_2 = Objet("sprites/bois_classique.2.png",window_width//2+100, window_height//2 - 25,0)
pièces = Objet("sprites/bois_piece.png",window_width//2+100, window_height//2+100,0)
pièces_2 = Objet("sprites/piece.2.png",window_width//2+100, window_height//2+100,0)
solo = Objet("sprites/solo_bois.png",window_width//2+100, window_height//2 + 225,0)
solo_2 = Objet("sprites/solo_bois.2.png",window_width//2+100, window_height//2 + 225,0)
boutons = [classique,classique_2,pièces,pièces_2,solo,solo_2]
fleche = Objet("sprites/fleche.png", 100, 100, 0)
coin = Objet("sprites/coin.png", random.randint(30,1290), random.randint(30,750), 0)

#création des images load
explosion_image= pygame.image.load("sprites/explosion.png")
background_image= pygame.image.load("sprites/background.3.png")

#création des variables de temps 
temps_apparition = time.time()
temps_change_boutons = time.time()
temps_rebound = time.time()

#création des sons
pygame.mixer.music.load("sons/jazz.mp3")
pygame.mixer.music.play(-1)
tir_song = pygame.mixer.Sound("sons/tir.mp3")

#création des variables utilisés dans le jeu
img_score=[img_0,img_1,img_2,img_3,img_4,img_5,img_6,img_7,img_8,img_9]
explosions = []
variable = 0
in_game = False
bullets = []
running = True
mode = 2

#Boucle principale
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT: #pour pouvoir quitter le jeu avec la croix
            running = False
    temps_actuel = time.time() #on définit une variable de temps qui s'actualise à chaque fraim
        
    if in_game: #quand un mode est lancé
        screen.fill((255, 255, 255)) #renplit l'écran en blanc
        screen.blit(img_back.image, img_back.rect) #affichage du background de jeu

        tank1.visualiser_score(-1) #fonction visualiser pour le tank1 puis tank2
        tank2.visualiser_score(1)
        
        #pygame.draw.rect(screen,(255,0,0),tank1)  #si besoin d'afficher la hitbox des tanks
        #pygame.draw.rect(screen,(255,0,255),tank2)

        if mode != 2: # On en enlève toute les fonctions du tank1 si c'est le mode solo
            tank1.move_tank(bool(0)) #pour dépllaçer le tank
            tank1.visualiser(tank1.angle) #visualiser le tank
            tank1.tire_ou_pas(bool(0)) #vérifier si le tank tire
            tank1.verif_bullet(0) #vérifier les colisions avec les bullets
            if tank1.explosion: #ce qui se passe si le tank est en état d'explosion
                if temps_actuel - tank1.temps_explosion <1.5: #on fait durer pendant 1.5s
                    tank1.bloque_tank() 
                else:    
                    tank1.debloque_tank()
                    if mode == 2: #si c'est le mode solo, le jeu s'arrète à la première mort donc on reinitialise les scores et retourne au menu
                        tank1.score, tank2.score = 0,0
                        in_game = False
        
        tank2.move_tank(bool(1)) #pareil avec le tank2
        tank2.visualiser(tank2.angle)
        tank2.tire_ou_pas(bool(1))
        tank2.verif_bullet(1)
        if tank2.explosion:
            if temps_actuel - tank2.temps_explosion <1.5:
                tank2.bloque_tank()
            else:
                tank2.debloque_tank()
                if mode == 2:
                    tank1.score, tank2.score = 0,0
                    in_game = False
        
        if mode == 1: #si c'est le mode pièce, on affiche la pièce et vérfie si un tank la prend
            screen.blit(coin.image, coin.rect)
            tank1.verif_obj()
            tank2.verif_obj()
        
        updated_bullets = []
        for bullet in bullets: #pour toutes les bullets de la liste
            bullet.move() #on les fait se déplacer
            updated_bullets.append(bullet) #on ajoute si un nouveau bullet à été créer
            screen.blit(bullet.image, bullet.rect)  #on affiche chaque bullet
        bullets = updated_bullets

        if tank1.score >= 5 or tank2.score >= 5 and not mode == 2: #on vérifie si un tank à dépassé les 5 points
                tank1.score, tank2.score = 0,0 #on réiinitialise les scores et retourne au menu
                in_game = False

        if fleche.cliquer(): #si la fleche est cliqué, on retourne au menu
            in_game =False
        screen.blit(fleche.image,fleche.rect) #affiche la fleche
        
    else: #lorsqu'on est au menu
        tank1.reinitialiser_jeu() #on reinitialise les tanks et leurs scores
        tank1.score, tank2.score = 0,0
        if temps_actuel - temps_change_boutons >0.2: #pour éviter le défilement trop rapide entre les boutons
            if pygame.key.get_pressed()[pygame.K_UP]: #si on appuie sur la fleche directionelle haut
                if variable == 0: #si on est sur le bouton du haut, on passe au bouton du bas
                    variable = 2
                else:
                    variable -= 1 #sinon on monte d'un bouton
                temps_change_boutons = temps_actuel #actualise la variable de temps
            if pygame.key.get_pressed()[pygame.K_DOWN]:#si on appuie sur la fleche directionelle bas
                if variable == 2: #si on est sur le bouton du bas, on passe au bouton du haut
                    variable = 0
                else:
                    variable += 1 #sinon on descend d'un bouton 
                temps_change_boutons = temps_actuel
        screen.blit(background_image, (0,0)) #affiche le background du menu
        if variable == 0: #selon le bouton séléctionné, on affiche son image séléctionné, et l'image de base des autres 
            screen.blit(boutons[1].image, boutons[1].rect)
        else:
            screen.blit(boutons[0].image, boutons[0].rect)
        if variable == 1:
            screen.blit(boutons[3].image, boutons[3].rect)
        else:
            screen.blit(boutons[2].image, boutons[2].rect)
        if variable == 2:
            screen.blit(boutons[5].image, boutons[5].rect)
        else:
            screen.blit(boutons[4].image, boutons[4].rect) 

        if pygame.key.get_pressed()[pygame.K_SPACE]: #si on appuie sur espace, le jeu se lance avec le mode qui correspond au bouton séléctionné
            mode = variable
            tank2.temps_dernier_tir = temps_actuel #pour que le tank2 ne tire pas automatiquement au lancement de la partie
            in_game = True

    pygame.display.flip() #met à jour l'écran avec les modifications apportées
    pygame.time.Clock().tick(60) #contrôle le nombre d'images par seconde

#Quitter Pygame
pygame.quit()
sys.exit()